import React from 'react'
import Nav from './Navigation.js'
import { View, Text, StyleSheet} from 'react-native'


const Home = () => {
  return (
    <View style = {styles.contanier}>
      <Text style = {styles.text}>
        Hello! Its a home page)
      </Text>
      <Nav style = {styles.nav} />    
    </View>
  )
}

export default Home

styles = StyleSheet.create({
  contanier: {
    flex: 1,
  }, 
  text: {
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 20,
    fontSize: 20,
  }, 
  nav: {
    alignItems: 'center'
  }
})
