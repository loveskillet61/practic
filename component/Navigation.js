import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Actions } from 'react-native-router-flux'

const Nav = () => {
  const goToAbout = () => {
    Actions.about()
  }
  const goToWhat = () => {
    Actions.what()
  }
  const goToHome = () => {
    Actions.home()
  }
  return (
    <View style = {styles.contanier}>
      <View style = {styles.navigation}>
      <Text 
      onPress = {goToHome}
      style = {styles.item}>
        Home
        </Text>
      <Text 
      onPress = {goToAbout}
      style = {styles.item}>
        About
      </Text>
      <Text 
      onPress = {goToWhat}
      style = {styles.item}>
        What
      </Text>
      </View>
    </View>
  )
}

export default Nav


const styles = StyleSheet.create({
  contanier: {
    flex: 1,
  },
  navigation: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }, 
  item: {
    padding: 5
  }
})

