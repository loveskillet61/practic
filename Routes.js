import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Home from './component/Home.js'
import About from './component/About.js'
import What from './component/What.js'

const Routes = () => (
 <Router>
 <Scene key = "root">
 <Scene key = "home" component = {Home} title = "Home" initial = {true} />
 <Scene key = "about" component = {About} title = "About" />
 <Scene key = "what" component = {What} title = "What" />
 </Scene>
 </Router>
)
export default Routes
